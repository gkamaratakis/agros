import { Component, OnInit } from '@angular/core';
import { crudService } from '../crud.service';
import { AngularFireAuth } from '@angular/fire/auth';
import 'firebase/auth';

import { AgrofieldsPage } from '../agrofields/agrofields.page';
@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.page.html',
  styleUrls: ['./expenses.page.scss'],
})
export class ExpensesPage implements OnInit {
    expenses: any;
     agrofields: any;
    
   expenseName: string;
   expenseAgrofieldName: string;
   expenseAgrofield: string;
   expenseAgrofieldID: string;
   expenseDate: string = new Date().toISOString();
 
   expenseComments: string;
   expenseAmount: string;
 uid: string;
   constructor(private crudService: crudService, public afAuth: AngularFireAuth) { }
  
   ngOnInit() {
     var user = this.afAuth.auth.currentUser;
     this.uid=user.uid;
    // this.expenseDate=
     /*afto edw einai gia alli douleia*/
     this.crudService.read_Agrofields().subscribe(data => {
  
         this.agrofields = data.map(e => {
           return {
             id: e.payload.doc.id,
             isEdit: false,
             name: e.payload.doc.data()['name'],
             location: e.payload.doc.data()['location'],
             comments: e.payload.doc.data()['comments'],
           };
         })
         console.log(this.agrofields);
    
       });
 
 
 
       this.crudService.read_Expenses().subscribe(data => {
  
         this.expenses = data.map(e => {
           return {
             id: e.payload.doc.id,
             isEdit: false,
             name: e.payload.doc.data()['name'],
             agrofieldName: e.payload.doc.data()['agrofieldName'],
             agrofieldID: e.payload.doc.data()['agrofieldID'],
             comments: e.payload.doc.data()['comments'],
             amount: e.payload.doc.data()['amount'],
             date: e.payload.doc.data()['date']
           };
         })
         console.log(this.expenses);
    
       });
   }
 
   hide:boolean = false;
   ngIfCtrl(){
     this.hide = !this.hide;
   }
 
   RemoveRecord(rowID) {
     if(confirm("Are you sure?")){this.crudService.delete_Expense(rowID);}
   }
 
   CreateRecord() {
     let record = {};
     record['agrofieldName'] = this.expenseAgrofieldName = this.expenseAgrofield["name"];
     record['agrofieldID'] = this.expenseAgrofieldID = this.expenseAgrofield["id"];
     record['amount'] = this.expenseAmount;
     record['comments'] = this.expenseComments;
     record['date'] = this.expenseDate;
     record['uid'] = this.uid;
     this.crudService.create_NewExpense(record).then(resp => {
       this.expenseName = "";
       this.expenseAgrofieldName = "";
       this.expenseAgrofieldID = "";
       this.expenseDate="";
       this.expenseComments = "";
       this.expenseAmount = "";
       console.log(resp);
     })
       .catch(error => {
         console.log(error);
       });
   }
 
   EditRecord(record) {
     record.isEdit = true;
     record.EditAmount = record.amount;
     record.EditComments = record.comments;
   }
   
   UpdateRecord(recordRow) {
     let record = {};
     record['amount'] = recordRow.EditAmount;
     record['comments'] = recordRow.EditComments;
     this.crudService.update_Expense(recordRow.id, record);
     recordRow.isEdit = false;
   }
 
   showMySelection(v):void {
     console.log(v);
   }
}
