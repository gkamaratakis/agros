import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {Router} from "@angular/router"
import { AuthGuardService as AuthGaurd } from '../services/auth-guard.service';

import { crudService } from '../crud.service';

export class User {
    email: string;
    password: string;
    repassword: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
    public user:User = new User();
  constructor(public afAuth: AngularFireAuth,private router: Router,private crudService: crudService) { }

  ngOnInit() {
  }
  /*
  CreateRecord() {
    let record = {};
    record['name'] = this.agrofieldName;
    record['location'] = this.agrofieldLocation;
    record['comments'] = this.agrofieldComments;
    this.crudService.create_NewAgrofield(record).then(resp => {
      this.agrofieldName = "";
      this.agrofieldLocation = "";
      this.agrofieldComments = "";
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }
  */
  async createAccount(){
    if(this.user.password===this.user.repassword){
        
        try{
            const res= await this.afAuth.auth.createUserWithEmailAndPassword(
                this.user.email,
                this.user.password
            ) ;
            let uid=res.user.uid;
            let email=res.user.email;
            let record = {};
            record['fname'] = "";
            record['lname'] = "";
            record['email'] = email;
            record['status'] = "";
            record['authID'] = uid;
            const res2=this.crudService.create_NewUser(record)
            this.router.navigate(['/home'])
            console.log(res);
            
        }catch(err){
            console.dir(err)
            if(err.code==="auth/invalid-email"){
                alert("Invalid email address")
            }else if(err.code==="auth/weak-password"){
                alert("Weak password given")
            }

        }
           
    }else{
        alert("Passwords do not match")
    }
}

}
