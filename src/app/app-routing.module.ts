import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from "./services/auth-guard.service";
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: "full"
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
    canActivate: [AuthGuardService]
  },
/*
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule),
    canActivate: [AuthGuardService]
  },
*/
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'agrofields',
    loadChildren: () => import('./agrofields/agrofields.module').then( m => m.AgrofieldsPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'plants',
    loadChildren: () => import('./plants/plants.module').then( m => m.PlantsPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'cultivations',
    loadChildren: () => import('./cultivations/cultivations.module').then( m => m.CultivationsPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'income',
    loadChildren: () => import('./income/income.module').then( m => m.IncomePageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'expenses',
    loadChildren: () => import('./expenses/expenses.module').then( m => m.ExpensesPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'stock',
    loadChildren: () => import('./stock/stock.module').then( m => m.StockPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'works',
    loadChildren: () => import('./works/works.module').then( m => m.WorksPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'care',
    loadChildren: () => import('./care/care.module').then( m => m.CarePageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'statistics',
    loadChildren: () => import('./statistics/statistics.module').then( m => m.StatisticsPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'production',
    loadChildren: () => import('./production/production.module').then( m => m.ProductionPageModule),
    canActivate: [AuthGuardService]
  }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
    exports: [RouterModule]
  })
export class AppRoutingModule {}
