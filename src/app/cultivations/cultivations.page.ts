import { Component, OnInit } from '@angular/core';
import { crudService } from '../crud.service';
import { AngularFireAuth } from '@angular/fire/auth';
import 'firebase/auth';

import { AgrofieldsPage } from '../agrofields/agrofields.page';
import { PlantsPage } from '../plants/plants.page';



@Component({
  selector: 'app-cultivations',
  templateUrl: './cultivations.page.html',
  styleUrls: ['./cultivations.page.scss'],
})
export class CultivationsPage implements OnInit {
    cultivations: any;
    agrofields: any;
    plants: any;
  cultivationName: string;
  cultivationAgrofieldName: string;
  cultivationAgrofield: string;
  cultivationAgrofieldID: string;
  cultivationPlant: string;
  cultivationPlantName: string;
  cultivationPlantID: string;
  cultivationComments: string;
  cultivationQuantity: string;
uid: string;
  constructor(private crudService: crudService, public afAuth: AngularFireAuth) { }
 
  ngOnInit() {
    var user = this.afAuth.auth.currentUser;
    this.uid=user.uid;
    /*afto edw einai gia alli douleia*/
    this.crudService.read_Agrofields().subscribe(data => {
 
        this.agrofields = data.map(e => {
          return {
            id: e.payload.doc.id,
            isEdit: false,
            name: e.payload.doc.data()['name'],
            location: e.payload.doc.data()['location'],
            comments: e.payload.doc.data()['comments'],
          };
        })
        console.log(this.agrofields);
   
      });
 /**/
 this.crudService.read_Plants().subscribe(data => {
 
    this.plants = data.map(e => {
      return {
        id: e.payload.doc.id,
        isEdit: false,
        name: e.payload.doc.data()['name']
      };
    })
    console.log(this.plants);

  });
/**/


      this.crudService.read_Cultivations().subscribe(data => {
 
        this.cultivations = data.map(e => {
          return {
            id: e.payload.doc.id,
            isEdit: false,
            name: e.payload.doc.data()['name'],
            agrofieldName: e.payload.doc.data()['agrofieldName'],
            agrofieldID: e.payload.doc.data()['agrofieldID'],
            plantName: e.payload.doc.data()['plantName'],
            plantID: e.payload.doc.data()['plantID'],
            comments: e.payload.doc.data()['comments'],
            quantity: e.payload.doc.data()['quantity'],
          };
        })
        console.log(this.cultivations);
   
      });
  }

  hide:boolean = false;
  ngIfCtrl(){
    this.hide = !this.hide;
  }

  RemoveRecord(rowID) {
    if(confirm("Are you sure?")){this.crudService.delete_Cultivation(rowID);}
  }

  CreateRecord() {
    let record = {};
    record['name'] = this.cultivationName;
    record['agrofieldName'] = this.cultivationAgrofieldName = this.cultivationAgrofield["name"];
    record['agrofieldID'] = this.cultivationAgrofieldID = this.cultivationAgrofield["id"];
    record['plantName'] = this.cultivationPlantName = this.cultivationPlant["name"];
    record['plantID'] = this.cultivationPlantID = this.cultivationPlant["id"];
    record['comments'] = this.cultivationComments;
    record['quantity'] = this.cultivationQuantity;
    record['uid'] = this.uid;
    this.crudService.create_NewCultivation(record).then(resp => {
      this.cultivationName = "";
      this.cultivationAgrofieldName = "";
      this.cultivationAgrofieldID = "";
      this.cultivationPlantName = "";
      this.cultivationPlantID = "";
      this.cultivationComments = "";
      this.cultivationQuantity = "";
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }

  EditRecord(record) {
    record.isEdit = true;
    record.EditName = record.name;
    record.EditQuantity = record.quantity;
    record.EditComments = record.comments;
  }
  
  UpdateRecord(recordRow) {
    let record = {};
    record['name'] = recordRow.EditName;
    record['quantity'] = recordRow.EditQuantity;
    record['comments'] = recordRow.EditComments;
    this.crudService.update_Cultivation(recordRow.id, record);
    recordRow.isEdit = false;
  }

  showMySelection(v):void {
    console.log(v);
  }

}
