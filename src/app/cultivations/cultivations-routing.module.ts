import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CultivationsPage } from './cultivations.page';

const routes: Routes = [
  {
    path: '',
    component: CultivationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CultivationsPageRoutingModule {}
