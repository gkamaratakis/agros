import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CultivationsPageRoutingModule } from './cultivations-routing.module';

import { CultivationsPage } from './cultivations.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CultivationsPageRoutingModule
  ],
  declarations: [CultivationsPage]
})
export class CultivationsPageModule {}
