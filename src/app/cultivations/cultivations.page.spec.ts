import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CultivationsPage } from './cultivations.page';

describe('CultivationsPage', () => {
  let component: CultivationsPage;
  let fixture: ComponentFixture<CultivationsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CultivationsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CultivationsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
