import { Component, OnInit } from '@angular/core';
import { crudService } from '../crud.service';
import { AngularFireAuth } from '@angular/fire/auth';
import 'firebase/auth';

@Component({
  selector: 'app-plants',
  templateUrl: './plants.page.html',
  styleUrls: ['./plants.page.scss'],
})
export class PlantsPage implements OnInit {
    plants: any;
    plantName: string;
    plantComments: string;
  uid: string;
    constructor(private crudService: crudService,public afAuth: AngularFireAuth) { }
   
    ngOnInit() {
      var user = this.afAuth.auth.currentUser;
      this.uid=user.uid;
      this.crudService.read_Plants().subscribe(data => {
   
          this.plants = data.map(e => {
            return {
              id: e.payload.doc.id,
              isEdit: false,
              name: e.payload.doc.data()['name'],
             
              comments: e.payload.doc.data()['comments'],
            };
          })
          console.log(this.plants);
     
        });
    }
  
    hide:boolean = false;
    ngIfCtrl(){
      this.hide = !this.hide;
    }
  
    RemoveRecord(rowID) {
      if(confirm("Are you sure?")){this.crudService.delete_Plant(rowID);}
    }
  
    CreateRecord() {
      let record = {};
      record['name'] = this.plantName;
      record['comments'] = this.plantComments;
      record['uid'] = this.uid;
      this.crudService.create_NewPlant(record).then(resp => {
        this.plantName = "";
       
        this.plantComments = "";
        console.log(resp);
      })
        .catch(error => {
          console.log(error);
        });
    }
  
    EditRecord(record) {
      record.isEdit = true;
      record.EditName = record.name;
      record.EditLocation = record.location;
      record.EditComments = record.comments;
    }
    
    UpdateRecord(recordRow) {
      let record = {};
      record['name'] = recordRow.EditName;
      record['comments'] = recordRow.EditComments;
      this.crudService.update_Plant(recordRow.id, record);
      recordRow.isEdit = false;
    }
}
