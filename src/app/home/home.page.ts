import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

import 'firebase/auth';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
    uid: string
  constructor(public afAuth: AngularFireAuth ) {  
        var user = this.afAuth.auth.currentUser;
        this.uid=user.uid
        //edw kanoume pass to UID stin home...etsi tha prepei kai stis alles
 }

}
