import { Component, OnInit } from '@angular/core';
import { crudService } from '../crud.service';
import { AngularFireAuth } from '@angular/fire/auth';

import 'firebase/auth';
import { Chart } from 'chart.js';
import { HttpClient } from '@angular/common/http';
import { ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
//import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from 'constants';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.page.html',
  styleUrls: ['./statistics.page.scss'],
})
export class StatisticsPage implements OnInit {
    incomes: number;
    stats : any;
    stats2 :any;
    incomesTotal: number;
  // Data
  chartData: ChartDataSets[] = [{ data: [], label: 'Income' }];
  chartLabels: Label[];
 
  // Options
  chartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'Incomes by date'
    },
    pan: {
      enabled: true,
      mode: 'xy'
    },
    zoom: {
      enabled: false,
      mode: 'xy'
    }
  };
  chartColors: Color[] = [
    {
      borderColor: '#000000',
      backgroundColor: '#0000ff'
    }
  ];
  chartType = 'bar';
  showLegend = false;
 
  // For search
  stock = '';
 
  constructor(private http: HttpClient,private crudService: crudService,public afAuth: AngularFireAuth) {
    this.incomesTotal= this.incomes
  }

  getData() {
    this.incomes=0;
    this.crudService.read_Incomes().subscribe(data => {

        this.chartLabels = [];
        this.chartData[0].data = [];
       
        this.stats = data.map(e => {
            return {
              id: e.payload.doc.id,
              isEdit: false,
              amount: e.payload.doc.data()['amount'],
              date: e.payload.doc.data()['date'],
              comments: e.payload.doc.data()['comments'],
              total: 0
            };
          })

          for (let entry of this.stats) {
            this.incomes+=parseFloat(entry.amount)
            //alert(this.incomes);
            //console.log(entry.date)
            this.chartLabels.push(entry.date);
            this.chartData[0].data.push(entry.amount);
          }
          
          this.incomesTotal= this.incomes
//console.log(this.stats)

      });
      
}

typeChanged(e) {
  const on = e.detail.checked;
  this.chartType = on ? 'line' : 'bar';
}
  ngOnInit() {
    this.getData();
    
  }

}


/*
 getData() {
    this.http.get(`https://financialmodelingprep.com/api/v3/historical-price-full/${this.stock}?from=2018-03-12&to=2019-03-12`).subscribe(res => {
    const history = res['historical'];

    this.chartLabels = [];
    this.chartData[0].data = [];

    for (let entry of history) {
      this.chartLabels.push(entry.date);
      this.chartData[0].data.push(entry['close']);
    }
  });
}
*/