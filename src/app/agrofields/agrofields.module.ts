import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AgrofieldsPageRoutingModule } from './agrofields-routing.module';

import { AgrofieldsPage } from './agrofields.page';



 
@Injectable({
  providedIn: 'root'
})


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgrofieldsPageRoutingModule
  ],
  declarations: [AgrofieldsPage]
})
export class AgrofieldsPageModule {
}
