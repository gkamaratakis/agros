import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgrofieldsPage } from './agrofields.page';

describe('AgrofieldsPage', () => {
  let component: AgrofieldsPage;
  let fixture: ComponentFixture<AgrofieldsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgrofieldsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgrofieldsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
