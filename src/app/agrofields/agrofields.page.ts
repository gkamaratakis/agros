import { Component, OnInit } from '@angular/core';
import { crudService } from '../crud.service';
import { AngularFireAuth } from '@angular/fire/auth';

import 'firebase/auth';
@Component({
  selector: 'app-agrofields',
  templateUrl: './agrofields.page.html',
  styleUrls: ['./agrofields.page.scss'],
})
export class AgrofieldsPage implements OnInit {
    agrofields: any;
  agrofieldName: string;
  agrofieldLocation: string;
  agrofieldComments: string;
uid: string;
  constructor(private crudService: crudService,public afAuth: AngularFireAuth) { }
 
  ngOnInit() {
    var user = this.afAuth.auth.currentUser;
    this.uid=user.uid;
    this.crudService.read_Agrofields().subscribe(data => {
 
        this.agrofields = data.map(e => {
          return {
            id: e.payload.doc.id,
            isEdit: false,
            name: e.payload.doc.data()['name'],
            location: e.payload.doc.data()['location'],
            comments: e.payload.doc.data()['comments'],
          };
        })
        console.log(this.agrofields);
   
      });
  }

  hide:boolean = false;
  ngIfCtrl(){
    this.hide = !this.hide;
  }

  RemoveRecord(rowID) {
    if(confirm("Are you sure?")){this.crudService.delete_Agrofield(rowID);}
  }

  CreateRecord() {
    let record = {};
    record['name'] = this.agrofieldName;
    record['location'] = this.agrofieldLocation;
    record['comments'] = this.agrofieldComments;
    record['uid'] = this.uid;
    this.crudService.create_NewAgrofield(record).then(resp => {
      this.agrofieldName = "";
      this.agrofieldLocation = "";
      this.agrofieldComments = "";
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }

  EditRecord(record) {
    record.isEdit = true;
    record.EditName = record.name;
    record.EditLocation = record.location;
    record.EditComments = record.comments;
  }
  
  UpdateRecord(recordRow) {
    let record = {};
    record['name'] = recordRow.EditName;
    record['location'] = recordRow.EditLocation;
    record['comments'] = recordRow.EditComments;
    this.crudService.update_Agrofield(recordRow.id, record);
    recordRow.isEdit = false;
  }
}
