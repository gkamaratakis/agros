import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgrofieldsPage } from './agrofields.page';

const routes: Routes = [
  {
    path: '',
    component: AgrofieldsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgrofieldsPageRoutingModule {}
