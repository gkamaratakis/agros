import { Component, OnInit } from '@angular/core';
import { crudService } from '../crud.service';
import { AngularFireAuth } from '@angular/fire/auth';
import 'firebase/auth';

import { AgrofieldsPage } from '../agrofields/agrofields.page';

@Component({
  selector: 'app-income',
  templateUrl: './income.page.html',
  styleUrls: ['./income.page.scss'],
})
export class IncomePage implements OnInit {
   incomes: any;
    agrofields: any;
   
  incomeName: string;
  incomeAgrofieldName: string;
  incomeAgrofield: string;
  incomeAgrofieldID: string;
  incomeDate: string = new Date().toISOString();

  incomeComments: string;
  incomeAmount: string;
uid: string;
  constructor(private crudService: crudService, public afAuth: AngularFireAuth) { }
 
  ngOnInit() {
    var user = this.afAuth.auth.currentUser;
    this.uid=user.uid;
   // this.incomeDate=
    /*afto edw einai gia alli douleia*/
    this.crudService.read_Agrofields().subscribe(data => {
 
        this.agrofields = data.map(e => {
          return {
            id: e.payload.doc.id,
            isEdit: false,
            name: e.payload.doc.data()['name'],
            location: e.payload.doc.data()['location'],
            comments: e.payload.doc.data()['comments'],
          };
        })
        console.log(this.agrofields);
   
      });



      this.crudService.read_Incomes().subscribe(data => {
 
        this.incomes = data.map(e => {
          return {
            id: e.payload.doc.id,
            isEdit: false,
            name: e.payload.doc.data()['name'],
            agrofieldName: e.payload.doc.data()['agrofieldName'],
            agrofieldID: e.payload.doc.data()['agrofieldID'],
            comments: e.payload.doc.data()['comments'],
            amount: e.payload.doc.data()['amount'],
            date: e.payload.doc.data()['date']
          };
        })
        console.log(this.incomes);
   
      });
  }

  hide:boolean = false;
  ngIfCtrl(){
    this.hide = !this.hide;
  }

  RemoveRecord(rowID) {
    if(confirm("Are you sure?")){this.crudService.delete_Income(rowID);}
  }

  CreateRecord() {
    let record = {};
    record['agrofieldName'] = this.incomeAgrofieldName = this.incomeAgrofield["name"];
    record['agrofieldID'] = this.incomeAgrofieldID = this.incomeAgrofield["id"];
    record['amount'] = this.incomeAmount;
    record['comments'] = this.incomeComments;
    record['date'] = this.incomeDate;
    record['uid'] = this.uid;
    this.crudService.create_NewIncome(record).then(resp => {
      this.incomeName = "";
      this.incomeAgrofieldName = "";
      this.incomeAgrofieldID = "";
      this.incomeDate="";
      this.incomeComments = "";
      this.incomeAmount = "";
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }

  EditRecord(record) {
    record.isEdit = true;
    record.EditAmount = record.amount;
    record.EditComments = record.comments;
  }
  
  UpdateRecord(recordRow) {
    let record = {};
    record['amount'] = recordRow.EditAmount;
    record['comments'] = recordRow.EditComments;
    this.crudService.update_Income(recordRow.id, record);
    recordRow.isEdit = false;
  }

  showMySelection(v):void {
    console.log(v);
  }
}
