import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
        title: 'Agrofields',
        url: '/agrofields',
        icon: 'map'
      },
      {
        title: 'Plants',
        url: '/plants',
        icon: 'leaf'
      },
      {
        title: 'Cultivations',
        url: '/cultivations',
        icon: 'analytics'
      },
      {
        title: 'Production',
        url: '/production',
        icon: 'ios-analytics'
      },
      {
        title: 'Incomes',
        url: '/income',
        icon: 'logo-euro'
      },
      {
        title: 'Expenses',
        url: '/expenses',
        icon: 'cart'
      },
      {
        title: 'Stock',
        url: '/stock',
        icon: 'folder-open'
      },
      {
        title: 'Works',
        url: '/works',
        icon: 'clock'
      },
      {
        title: 'Care',
        url: '/care',
        icon: 'bug'
      },
      /*
      {
        title: 'To-Do',
        url: '/list',
        icon: 'list'
      },
      */
      {
        title: 'Statistics',
        url: '/statistics',
        icon: 'pie'
      },
      {
        title: 'Settings',
        url: '/settings',
        icon: 'build'
      },
      {
        title: 'Logout',
        url: '/login',
        icon: 'log-out'
      }
    
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
