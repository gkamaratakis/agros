import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {Router} from "@angular/router"
import { AuthGuardService as AuthGaurd } from '../services/auth-guard.service';
export class User {
    email: string;
    password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
   
public user:User = new User();
    constructor(public afAuth: AngularFireAuth,private router: Router) { }

    ngOnInit() {
         this.logout();
    }

    async login(){
       try{
            const user= await this.afAuth.auth.signInWithEmailAndPassword(this.user.email,this.user.password);
            this.router.navigate(['/home'])
            //console.log(user);
       }catch(err){
            if(err.code==="auth/user-not-found"){
                alert("User not found ! ")
            }
            else if (err.code === 'auth/wrong-password')
            {
                alert('Wrong password.');
            }/*
            else{
                alert(err.code)
            } */
       }
    }
    
     
    
    async logout(){
        this.afAuth.auth.signOut();
        //alert("ekana logout");
    }
    
}
