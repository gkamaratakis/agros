import { Injectable } from '@angular/core';

import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';

import { AngularFireAuth } from '@angular/fire/auth';

import 'firebase/auth';

@Injectable()

export class AuthGuardService implements CanActivate {

    constructor(public afAuth: AngularFireAuth,public router: Router) {}

    canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {

        return new Promise((resolve, reject) => {

            this.afAuth.auth.onAuthStateChanged(user => {
                if (user) {
                    //alert(user.email);
                    // User is signed in.
                    resolve(true);
                } else {
                    // No user is signed in.
                    resolve(false);
                    this.router.navigate(['login']);
                }
            });
        });
    }
}