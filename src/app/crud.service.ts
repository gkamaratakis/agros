import { Injectable } from '@angular/core';
 
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

import 'firebase/auth';
@Injectable({
  providedIn: 'root'
})
export class crudService {
    uid: string
    constructor(
        public afAuth: AngularFireAuth,
      private firestore: AngularFirestore
    ) {
        var user = this.afAuth.auth.currentUser;
        this.uid=user.uid        
     }
    /*===========USERS============ */ 
     
    create_NewUser(record) {
        return this.firestore.collection('users').add(record);
      }
      
   /*===========AGROFIELDS============ */
    create_NewAgrofield(record) {
      return this.firestore.collection('agrofields').add(record);
    }
   
    read_Agrofields() {
        return this.firestore.collection('agrofields',ref => ref.where('uid', '==',this.uid )).snapshotChanges();
      }
   
    update_Agrofield(recordID,record){
      this.firestore.doc('agrofields/' + recordID).update(record);
    }
   
    delete_Agrofield(record_id) {
      this.firestore.doc('agrofields/' + record_id).delete();
    }



    /*===========PLANTS============ */
    create_NewPlant(record) {
        return this.firestore.collection('plants').add(record);
      }
     
      read_Plants() {
          return this.firestore.collection('plants',ref => ref.where('uid', '==',this.uid )).snapshotChanges();
        }
     
      update_Plant(recordID,record){
        this.firestore.doc('plants/' + recordID).update(record);
      }
     
      delete_Plant(record_id) {
        this.firestore.doc('plants/' + record_id).delete();
      }
  
       /*===========CULTIVATIONS============ */
    create_NewCultivation(record) {
        return this.firestore.collection('cultivations').add(record);
      }
     
      read_Cultivations() {
          return this.firestore.collection('cultivations',ref => ref.where('uid', '==',this.uid )).snapshotChanges();
        }
     
      update_Cultivation(recordID,record){
        this.firestore.doc('cultivations/' + recordID).update(record);
      }
     
      delete_Cultivation(record_id) {
        this.firestore.doc('cultivations/' + record_id).delete();
      }
  
        /*===========INCOMES============ */
    create_NewIncome(record) {
        return this.firestore.collection('incomes').add(record);
      }
     
      read_Incomes() {
          return this.firestore.collection('incomes',ref => ref.where('uid', '==',this.uid )).snapshotChanges();
        }
     
      update_Income(recordID,record){
        this.firestore.doc('incomes/' + recordID).update(record);
      }
     
      delete_Income(record_id) {
        this.firestore.doc('incomes/' + record_id).delete();
      }

         /*===========EXPENSES============ */
    create_NewExpense(record) {
        return this.firestore.collection('expenses').add(record);
      }
     
      read_Expenses() {
          return this.firestore.collection('expenses',ref => ref.where('uid', '==',this.uid )).snapshotChanges();
        }
     
      update_Expense(recordID,record){
        this.firestore.doc('expenses/' + recordID).update(record);
      }
     
      delete_Expense(record_id) {
        this.firestore.doc('expenses/' + record_id).delete();
      }
}