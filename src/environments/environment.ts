// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyASMeTqcwrkkVmaO5H_4e0L4n0eBWZOjjs",
    authDomain: "agro-a7785.firebaseapp.com",
    databaseURL: "https://agro-a7785.firebaseio.com",
    projectId: "agro-a7785",
    storageBucket: "agro-a7785.appspot.com",
    messagingSenderId: "921679969018",
    appId: "1:921679969018:web:91fde98c1531f9b7f2a743"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
